# gpiodemo

Demo that basically shows how to read a gpio pin on a raspberry pi and then send
an HTTP post to some server. Simple.

## How to use the project

The project basically contains two files:

`server.php`: a simple php script that just returns the '`name`' from a post

`client.py`: a simply client, that sends a post whenever a gpio pin is switched
`ON`.

The `client.py` has been tested on a raspberry pi 3B with a "faily recent"
Raspberian (10.4).

## Needed libraries

The `client.py` needs two libraries:

* `requests`: this should be installed in raspberian by default. If not, then
  install it by:

    ```
    sudo apt install python3-requests
    ```

* `gpiozero`: this is used to read from a gpio pin. This probably needs to be
  installed:

    ```
    sudo apt install python3-gpiozero
    ```

The `server.php` runs on a standard php installation. It's quite simple, but just
in case: it's tested on a debian 10.6.
