from gpiozero import Button
from signal import pause
import requests

url = "http://<server>/server.php"
params = {'name': 'raspberry pi'}

def sendPost():
  print("Sending post...")
  print(requests.post(url, data = params).text)

# Main: wait for "button" press (on GPIO17)
button = Button(17)
button.when_pressed = sendPost
pause()
